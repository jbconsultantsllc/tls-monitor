#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'Jason Brown'
__email__ = 'jason.brown@aptiv.com'
__date__ = '20210218'

import ssl, datetime, socket, csv
from pprint import pprint
from datetime import datetime

def certificate(host, port):

    f = open('tls_expiration.txt', 'a')
    f.write('Website: ' + host + ' - ')
    ssl_date_fmt = r'%b %d %H:%M:%S %Y %Z'
    context = ssl.create_default_context()
    conn = context.wrap_socket(
        socket.socket(socket.AF_INET),
        server_hostname=host,
    )
    try:
        conn.settimeout(3.0)
        conn.connect((host, port))
        ssl_info = conn.getpeercert()
        extime = datetime.strptime(ssl_info['notAfter'], ssl_date_fmt)
        f.write(str(extime))
    except:
        f.write(' - **** Exceeded TLS Socket Timeout ****')
        pass
    f.write('\n')
    f.close()

def main():
    with open('tls_servers.csv', 'r') as file:
        reader = csv.reader(file)
        for i in reader:
            certificate(i[0], int(i[1]))

if __name__ == '__main__':
    main()